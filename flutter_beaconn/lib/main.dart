import 'dart:io';

import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutter_beacon/flutter_beacon.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);


  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final StreamController<BluetoothState> streamController = StreamController();
  StreamSubscription<BluetoothState> _streamBluetooth;
  StreamSubscription<RangingResult> _streamRanging;
  final _regionBeacons = <Region, List<Beacon>>{};
  final _beacons = <Beacon>[];
  bool authorizationStatusOk = false;
  bool locationServiceEnabled = false;
  bool bluetoothEnabled = false;
  final regions = <Region>[
    Region(
      identifier: 'Cubeacon',
      proximityUUID: 'CB10023F-A318-3394-4199-A8730C7C1AEC',
    ),
  ];
@override
  void initState() {
  check();
    super.initState();
  }
  int _compareParameters(Beacon a, Beacon b) {
    int compare = a.proximityUUID.compareTo(b.proximityUUID);

    if (compare == 0) {
      compare = a.major.compareTo(b.major);
    }

    if (compare == 0) {
      compare = a.minor.compareTo(b.minor);
    }

    return compare;
  }
check() async{
  flutterBeacon.initializeScanning;

_streamRanging=  flutterBeacon.ranging(regions).listen((RangingResult result) {
  if (result != null && mounted) {
    setState(() {
      _regionBeacons[result.region] = result.beacons;
      _beacons.clear();
      _regionBeacons.values.forEach((list) {
        _beacons.addAll(list);
        print('info $list');
      });
      _beacons.sort(_compareParameters);
    });
  }
  });
  flutterBeacon.close;
}
  @override
  Widget build(BuildContext context) {
         return Scaffold(

           body:SafeArea(
           child: Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: <Widget>[
               Container(
                 width: 100.0,
                 height: 50.0,
                 color: Colors.blue,
                 child: InkWell(
                   child: Text("Scan"),
                   onTap: (){
                     setState(() {
                       flutterBeacon.initializeScanning;
                       if (Platform.isIOS) {
                         regions.add(Region(
                             identifier: 'Apple Airlocate',
                             proximityUUID: 'E2C56DB5-DFFB-48D2-B060-D0F5A71096E0'));
                       } else {
                         // android platform, it can ranging out of beacon that filter all of Proximity UUID
                         regions.add(Region(identifier: 'com.beacon'));
                       }
                       _streamRanging = flutterBeacon.ranging(regions).listen((RangingResult result) {
                         if(result == null){
                           print('null');
                         }
                         else{
                           print('ok');
                         }
                       });

                     });
                   },
                 ),
               ),
               Container(
                 width: 100.0,
                 height: 50.0,
                 color: Colors.blue,
                 child: InkWell(
                   child: Text('Stop'),
                   onTap: (){
                     setState(() {
                       _streamRanging.cancel();
                     });
                   },
                 ),
               )
             ],
           )

           )

         );

  }
}
