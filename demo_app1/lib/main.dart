import 'package:android_intent/flag.dart';
import 'package:flutter/material.dart';
import 'package:android_intent/android_intent.dart';
import 'package:platform/platform.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  Platform flat;

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  static const String routeName = "/explicitIntents";
  void _createAlarm() {
    final AndroidIntent intent = const AndroidIntent(
      action: 'android.intent.action.SET_ALARM',
      arguments: <String, dynamic>{
        'android.intent.extra.alarm.DAYS': <int>[2, 3, 4, 5, 6],
        'android.intent.extra.alarm.HOUR': 21,
        'android.intent.extra.alarm.MINUTES': 30,
        'android.intent.extra.alarm.SKIP_UI': true,
        'android.intent.extra.alarm.MESSAGE': 'Create a Flutter app',
      },
    );
    intent.launch();
  }
  void _openExplicitIntentsView(BuildContext context) {
    Navigator.of(context).pushNamed(routeName);
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(

        title: Text(widget.title),
      ),
      body: Center(

        child: Column(

          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'App 1',
            ),
            RaisedButton(
                child: const Text('Tap here to test explicit intents.'),
                onPressed: () => _openExplicitIntentsView(context)),
            RaisedButton(
              child: const Text(
                  'Tap here to display new app.'),
              onPressed: _testExplicitIntentFallback,
            ),
            RaisedButton(
              child: const Text(
                  'alarm.'),
              onPressed: _createAlarm,
            ),

          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          var platform;
          if (platform.isAndroid) {
            final AndroidIntent intent = AndroidIntent(
              action: 'action_application_details_settings',
              data: 'com.example.demoapp1', // replace com.example.app with your applicationId
            );
            await intent.launch();
          }

        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
  void _testExplicitIntentFallback() {
    final AndroidIntent intent = AndroidIntent(
        action: 'action_view',
        data: Uri.encodeFull('demo://demo.flutter.dev/test'),

        package: 'dev.flutter.deep_links_flutter');
    intent.launch();
  }

}
