import 'dart:async';
import 'package:beacon_broadcast/beacon_broadcast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  static const UUID = '39ED98FF-2900-441A-802F-9C398FC199D2';
  static const MAJOR_ID = 2;
  static const MINOR_ID = 98;
  static const TRANSMISSION_POWER = -198;
  static const IDENTIFIER = 'test_beacon';
  static const LAYOUT = BeaconBroadcast.EDDYSTONE_UID_LAYOUT;
  static const MANUFACTURER_ID = 0x0198;

  BeaconBroadcast beaconBroadcast = BeaconBroadcast();
  BeaconStatus _isTransmissionSupported;
  bool _isAdvertising = false;
  StreamSubscription<bool> _isAdvertisingSubscription;
  String str;

  @override
  void initState() {
    super.initState();
    _isAdvertisingSubscription =
        beaconBroadcast.getAdvertisingStateChange().listen((isAdvertising) {
      setState(() {
        _isAdvertising = isAdvertising;
      });
    });
    checkBeacon();
  }

  checkBeacon() async {
    var transmissionSupportStatus =
        await beaconBroadcast.checkTransmissionSupported();
    beaconBroadcast
        .checkTransmissionSupported()
        .then((isTransmissionSupported) {
      setState(() {
        _isTransmissionSupported = isTransmissionSupported;
        if (_isTransmissionSupported == BeaconStatus.SUPPORTED) {
          str = "Bluetooth On";
        } else {
          str = "Blutooth Off";
        }
      });
    });
//  switch (transmissionSupportStatus) {
//    case BeaconStatus.SUPPORTED:
//        str= "Bluetooth turn on";
//      break;
//    case BeaconStatus.NOT_SUPPORTED_MIN_SDK:
//      str= "Your Android system version is too low (min. is 21)";
//      break;
//    case BeaconStatus.NOT_SUPPORTED_BLE:
//    str = "Your device doesn't support BLE";
//      break;
//    case BeaconStatus.NOT_SUPPORTED_CANNOT_GET_ADVERTISER:
//      str="Either your chipset or driver is incompatible";
//      break;
//  }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Beacon Broadcast'),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Is transmission supported? ',
                    style: Theme.of(context).textTheme.headline),
                Text('$str', style: Theme.of(context).textTheme.subhead),
                Container(height: 16.0),
                Text('Is beacon started?',
                    style: Theme.of(context).textTheme.headline),
                _isAdvertising ? Text('On') : Text('Off'),
                Container(height: 16.0),
                Center(
                  child: RaisedButton(
                    onPressed: () {
                      beaconBroadcast
                          .setUUID(UUID)
                          .setMajorId(MAJOR_ID)
                          .setMinorId(MINOR_ID)
                          .setTransmissionPower(-59)
                          .setIdentifier(IDENTIFIER)
                          .setLayout(LAYOUT)
                          .setManufacturerId(MANUFACTURER_ID)
                          .start();
                    },
                    child: Text('START'),
                  ),
                ),
                Center(
                  child: RaisedButton(
                    onPressed: () {
                      beaconBroadcast.stop();
                    },
                    child: Text('STOP'),
                  ),
                ),
                Text('Beacon Data',
                    style: Theme.of(context).textTheme.headline),
                _isAdvertising
                    ? Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('UUID: $UUID'),
                          Text('Major id: $MAJOR_ID'),
                          Text('Minor id: $MINOR_ID'),
                          Text('Tx Power: $TRANSMISSION_POWER'),
                          Text('Identifier: $IDENTIFIER'),
                          Text('Layout: $LAYOUT'),
                          Text('Manufacturer Id: $MANUFACTURER_ID'),
                        ],
                      )
                    : Text('Null')
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    if (_isAdvertisingSubscription != null) {
      _isAdvertisingSubscription.cancel();
    }
  }
}
