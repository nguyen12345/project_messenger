import 'dart:convert';
import 'dart:async' show Future;
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}
const baseUrl = "https://jsonplaceholder.typicode.com/users";


class User {
  int id;
  String name;
  String email;
  String username;
  User(
    this.id,
    this.name,
    this.email,
    this.username,
  );
//  factory User.fromJson(Map<String, dynamic> json){
//    return new User(
//     id: json["id"],
//     name: json["name"],
//      email: json["email"],
//    );
//    }
//  Map toJson() {
//    return {'id': id, 'name': name, 'email': email};
//  }
  }




class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Future<List<User>> getJsonData() async{
   var response = await http.get(baseUrl);
   var fechData = json.decode(response.body);
   List<User> users=[];
   for(var u in fechData){
     User user = User(u["id"], u["name"], u["email"],u["username"]);
     users.add(user);
   }
   print(users.length);
   return users;

  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Sample App"),
      ),
      body:Container(
        child: FutureBuilder(
            future:getJsonData(),
            builder: (context,AsyncSnapshot snap){
             return ListView.builder(
                  itemCount: snap.data.length,
                  itemBuilder: (context,int i){
                    return new Column(
                      children: <Widget>[
                         Text(snap.data[i].username,style: TextStyle(color: Colors.amber),),
                         Text(snap.data[i].name)

                      ],
                    );
                  }
              );
            }
        
        )

      )
    );
  }

}
