import 'package:facebookmessenger/contains/base_url.dart';
import 'package:facebookmessenger/model/response/chat_response.dart';

import 'base_provider.dart';

class ChatProvider extends BaseProvider {
  Future<ChatResponse> getListChat({String uri}) async {
    if (uri == null) {
      uri = BaseUrl.baseUrl;
    }
    var jsonData = await this.get(uri);
    return ChatResponse.fromJson(jsonData);
  }
}
