import 'dart:core' as prefix0;
import 'dart:core';
import 'package:facebookmessenger/bloc/chat/chat_event.dart';
import 'package:facebookmessenger/bloc/chat/chat_state.dart';
import 'package:facebookmessenger/model/chat.dart';
import 'package:facebookmessenger/provider/chat_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ChatBloc extends Bloc<ChatEvent, ChatState> {
  List<Chat> listData = [];
  String nextUrl = '';

  ChatProvider _chatProvider = new ChatProvider();

  @override
  get initialState => InitalChat();

  @override
  Stream<ChatState> mapEventToState(ChatEvent event) async* {
    if (event is LoadChat) {
      if (state is InitalChat) {
          var response = await _chatProvider.getListChat();
          if (response.status == "success") {
            this.listData = response.data;
            prefix0.print(listData.length);
            //  nextUrl = response.nextUrl;
            yield ChatLoaded();
          }

      }
    }
  }
}
