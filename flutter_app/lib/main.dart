import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  bool isSend = true;
  bool newMess = true;
  bool iSeen = true;
  bool isOnline = true;
  String str = 'Chats';
  bool checkMess = true;
  bool checkContact = false;
  bool checkTab = true;
  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  _UIContact() {
    return Container(
      child: CustomScrollView(slivers: <Widget>[
        SliverToBoxAdapter(
          child: Padding(
            padding: const EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    setState(() {
                      checkTab = true;
                      print(checkTab);
                    });
                  },
                  child: checkTab
                      ? Container(
                    alignment: Alignment.center,
                    width: 130.0,
                    height: 36.0,
                    decoration: BoxDecoration(
                        color: Colors.grey[300],
                        borderRadius:
                        BorderRadius.all(Radius.circular(23.0))),
                    child: Text(
                      'TIN(22)',
                      style: TextStyle(
                          fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  )
                      : Container(
                    alignment: Alignment.center,
                    width: 130.0,
                    height: 36.0,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius:
                        BorderRadius.all(Radius.circular(23.0))),
                    child: Text(
                      'TIN(22)',
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                ),
                Expanded(
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        checkTab = false;
                        print(checkTab);
                      });
                    },
                    child: checkTab
                        ? Container(
                      alignment: Alignment.center,
                      height: 36.0,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                          BorderRadius.all(Radius.circular(23.0))),
                      child: Text(
                        'ĐANG HOẠT ĐỘNG',
                        style: TextStyle(fontSize: 16),
                      ),
                    )
                        : Container(
                      alignment: Alignment.center,
                      height: 36.0,
                      decoration: BoxDecoration(
                          color: Colors.grey[300],
                          borderRadius:
                          BorderRadius.all(Radius.circular(23.0))),
                      child: Text(
                        'ĐANG HOẠT ĐỘNG',
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        checkTab
            ? SliverToBoxAdapter(
          child: Padding(
            padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
            child: Container(
              height: 85.0,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: 20,
                  itemBuilder: (context, int index) {
                    return index == 0
                        ? Padding(
                      padding: const EdgeInsets.only(
                          left: 8.0, right: 6.0),
                      child: Container(
                        width: 74.0,
                        height: double.infinity,
                        child: Column(
                          children: <Widget>[
                            Container(
                              width: 50.0,
                              height: 56.0,
                              child: Icon(Icons.add),
                              decoration: BoxDecoration(
                                  color: Colors.grey[300],
                                  shape: BoxShape.circle),
                            ),
                            Text(
                              'Tin của bạn',
                              style: TextStyle(fontSize: 13.0),
                            )
                          ],
                        ),
                      ),
                    )
                        : Padding(
                      padding: const EdgeInsets.only(right: 16.0),
                      child: index == 1
                          ? Container(

                        // alignment: Alignment.topCenter,
                          width: 55.0,
                          child: Column(
                            children: <Widget>[
                              Stack(
                                  alignment: Alignment.center,
                                  children: <Widget>[
                                    Container(
                                      height: 54.0,
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        border: Border.all(
                                            color: Colors.blue,
                                            width: 3.0),
                                        shape: BoxShape.circle,
                                      ),
                                    ),
                                    CircleAvatar(
                                        radius: 22.0,
                                        backgroundImage:
                                        NetworkImage(
                                          'https://znews-photo.zadn.vn/w660/Uploaded/spuocaw/2017_10_04/KateWinsletinAvatar.jpg',
                                        )),
                                    Positioned(
                                      top: 39,
                                      left: 38,
                                      child: Stack(
                                        alignment:
                                        Alignment.center,
                                        children: <Widget>[
                                          Container(
                                            width: 15.0,
                                            height: 15.0,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                new BorderRadius
                                                    .circular(
                                                    25.0),
                                                border: Border.all(
                                                    color: Colors
                                                        .white,
                                                    width: 2.0),
                                                color: Colors
                                                    .grey[300]),
                                          ),
                                          Container(
                                            height: 12.0,
                                            width: 12.0,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                new BorderRadius
                                                    .circular(
                                                    25.0),
                                                color: Colors
                                                    .green),
                                          )
                                        ],
                                      ),
                                    ),
                                  ]),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 3.0),
                                child: Text('User $index'),
                              )
                            ],
                          ))
                          : Container(

                        // alignment: Alignment.topCenter,
                          width: 50.0,
                          child: Column(
                            children: <Widget>[
                              Stack(
                                //   alignment: Alignment.bottomRight,
                                  children: <Widget>[
                                    CircleAvatar(
                                        radius: 26.0,
                                        backgroundImage:
                                        NetworkImage(
                                          'https://znews-photo.zadn.vn/w660/Uploaded/spuocaw/2017_10_04/KateWinsletinAvatar.jpg',
                                        )),
                                    Positioned(
                                      top: 38,
                                      left: 36,
                                      child: Stack(
                                        alignment:
                                        Alignment.center,
                                        children: <Widget>[
                                          Container(
                                            width: 15.0,
                                            height: 15.0,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                new BorderRadius
                                                    .circular(
                                                    25.0),
                                                border: Border.all(
                                                    color: Colors
                                                        .white,
                                                    width: 2.0),
                                                color: Colors
                                                    .grey[300]),
                                          ),
                                          Container(
                                            height: 12.0,
                                            width: 12.0,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                new BorderRadius
                                                    .circular(
                                                    25.0),
                                                color: Colors
                                                    .green),
                                          )
                                        ],
                                      ),
                                    ),
                                  ]),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 3.0),
                                child: Text('User $index'),
                              )
                            ],
                          )),
                    );
                  }),
            ),
          ),
        )
            : SliverFixedExtentList(
          itemExtent: 70.0,
          delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int index) {
                return Padding(
                    padding: const EdgeInsets.only(left: 16.0, top: 16.0),
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          newMess = false;
                        });
                      },
                      child: Container(
                          height: 102.0,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                height: double.infinity,
                                child: Stack(children: <Widget>[
                                  Container(
                                    width: 66.0,
                                    height: 70,
                                  ),
                                  Positioned(
                                    child: Stack(
                                      overflow: Overflow.visible,
                                      alignment: Alignment.bottomRight,
                                      children: <Widget>[
                                        CircleAvatar(
                                            radius: 22,
                                            backgroundImage: NetworkImage(
                                              'https://upload.wikimedia.org/wikipedia/vi/thumb/0/09/MOS_1280_kalel.jpg/300px-MOS_1280_kalel.jpg',
                                            )),
                                        Positioned(
                                          top: 30,
                                          left: 34,
                                          child: Stack(
                                            alignment: Alignment.center,
                                            children: <Widget>[
                                              Container(
                                                width: 15.0,
                                                height: 15.0,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                    new BorderRadius
                                                        .circular(
                                                        25.0),
                                                    border: Border.all(
                                                        color:
                                                        Colors.white,
                                                        width: 2.0),
                                                    color:
                                                    Colors.grey[300]),
                                              ),
                                              Container(
                                                height: 12.0,
                                                width: 12.0,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                    new BorderRadius
                                                        .circular(
                                                        25.0),
                                                    color: Colors.green),
                                              )
                                            ],
                                          ),
                                        ),

                                      ],
                                    ),
                                  ),
                                ]),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 16.0),
                                  child: Container(
                                    alignment: Alignment.centerLeft,
                                    child: Column(
                                      mainAxisAlignment:
                                      MainAxisAlignment.center,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              bottom: 3.0),
                                          child: Text(
                                            'User $index',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16),
                                          ),
                                        ),
                                        index == 1
                                            ? Text(
                                          'User $index đang hoạt động',
                                          style: TextStyle(
                                              fontWeight:
                                              FontWeight.w300,
                                              fontSize: 12),
                                        )
                                            : Text(
                                          '',
                                          style: TextStyle(
                                              fontSize: 15),
                                        )

                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                  padding:
                                  const EdgeInsets.only(right: 16.0),
                                  child: Container(
                                      width: 38.0,
                                      height: 38.0,
                                      decoration: BoxDecoration(
                                          color: Colors.grey[200], shape: BoxShape.circle),
                                      child: Container(
                                          child: Icon(
                                            Icons.local_phone,
                                            color: Colors.black,
                                            size: 16.0,
                                          )))
                              ),
                            ],
                          )),
                    ));
              }, childCount: 50),
        ),
      ]),
    );
  }

  _UiMess() {
    return SafeArea(
        child: CustomScrollView(
          slivers: <Widget>[
            SliverToBoxAdapter(
                child: Padding(
                  padding: const EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
                  child: Container(
                    height: 40.0,
                    decoration: BoxDecoration(
                      color: Colors.grey[300],
                      borderRadius: BorderRadius.circular(40.0),
                    ),
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(Icons.search),
                        ),
                        Expanded(
                          child: Container(
                            child: TextField(
                              decoration: InputDecoration(
                                  border: InputBorder.none, hintText: 'Tìm kiếm'),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                )),
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                child: Container(
                  height: 85.0,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: 20,
                      itemBuilder: (context, int index) {
                        return index == 0
                            ? Padding(
                          padding:
                          const EdgeInsets.only(left: 8.0, right: 6.0),
                          child: Container(
                            width: 74.0,
                            height: double.infinity,
                            child: Column(
                              children: <Widget>[
                                Container(
                                  width: 50.0,
                                  height: 56.0,
                                  child: Icon(Icons.add),
                                  decoration: BoxDecoration(
                                      color: Colors.grey[300],
                                      shape: BoxShape.circle),
                                ),
                                Text(
                                  'Tin của bạn',
                                  style: TextStyle(fontSize: 13.0),
                                )
                              ],
                            ),
                          ),
                        )
                            : Padding(
                          padding: const EdgeInsets.only(right: 16.0),
                          child: index == 1
                              ? Container(

                            // alignment: Alignment.topCenter,
                              width: 55.0,
                              child: Column(
                                children: <Widget>[
                                  Stack(
                                      alignment: Alignment.center,
                                      children: <Widget>[
                                        Container(
                                          height: 54.0,
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            border: Border.all(
                                                color: Colors.blue,
                                                width: 3.0),
                                            shape: BoxShape.circle,
                                          ),
                                        ),
                                        CircleAvatar(
                                            radius: 22.0,
                                            backgroundImage: NetworkImage(
                                              'https://znews-photo.zadn.vn/w660/Uploaded/spuocaw/2017_10_04/KateWinsletinAvatar.jpg',
                                            )),
                                        Positioned(
                                          top: 39,
                                          left: 38,
                                          child: Stack(
                                            alignment: Alignment.center,
                                            children: <Widget>[
                                              Container(
                                                width: 15.0,
                                                height: 15.0,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                    new BorderRadius
                                                        .circular(
                                                        25.0),
                                                    border: Border.all(
                                                        color:
                                                        Colors.white,
                                                        width: 2.0),
                                                    color:
                                                    Colors.grey[300]),
                                              ),
                                              Container(
                                                height: 12.0,
                                                width: 12.0,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                    new BorderRadius
                                                        .circular(
                                                        25.0),
                                                    color: Colors.green),
                                              )
                                            ],
                                          ),
                                        ),
                                      ]),
                                  Padding(
                                    padding:
                                    const EdgeInsets.only(top: 3.0),
                                    child: Text('User $index'),
                                  )
                                ],
                              ))
                              : Container(

                            // alignment: Alignment.topCenter,
                              width: 50.0,
                              child: Column(
                                children: <Widget>[
                                  Stack(
                                    //   alignment: Alignment.bottomRight,
                                      children: <Widget>[
                                        CircleAvatar(
                                            radius: 26.0,
                                            backgroundImage: NetworkImage(
                                              'https://znews-photo.zadn.vn/w660/Uploaded/spuocaw/2017_10_04/KateWinsletinAvatar.jpg',
                                            )),
                                        Positioned(
                                          top: 38,
                                          left: 36,
                                          child: Stack(
                                            alignment: Alignment.center,
                                            children: <Widget>[
                                              Container(
                                                width: 15.0,
                                                height: 15.0,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                    new BorderRadius
                                                        .circular(
                                                        25.0),
                                                    border: Border.all(
                                                        color:
                                                        Colors.white,
                                                        width: 2.0),
                                                    color:
                                                    Colors.grey[300]),
                                              ),
                                              Container(
                                                height: 12.0,
                                                width: 12.0,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                    new BorderRadius
                                                        .circular(
                                                        25.0),
                                                    color: Colors.green),
                                              )
                                            ],
                                          ),
                                        ),
                                      ]),
                                  Padding(
                                    padding:
                                    const EdgeInsets.only(top: 3.0),
                                    child: Text('User $index'),
                                  )
                                ],
                              )),
                        );
                      }),
                ),
              ),
            ),
            SliverFixedExtentList(
              itemExtent: 70.0,
              delegate:
              SliverChildBuilderDelegate((BuildContext context, int index) {
                return Padding(
                    padding: const EdgeInsets.only(left: 16.0, bottom: 16.0),
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          newMess = false;
                        });
                      },
                      child: Container(
                          height: 102.0,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                height: double.infinity,
                                child: Stack(children: <Widget>[
                                  Container(
                                    width: 66.0,
                                    height: 70,
                                  ),
                                  Positioned(
                                    child: Stack(
                                      overflow: Overflow.visible,
                                      alignment: Alignment.bottomRight,
                                      children: <Widget>[
                                        CircleAvatar(
                                            radius: 26,
                                            backgroundImage: NetworkImage(
                                              'https://upload.wikimedia.org/wikipedia/vi/thumb/0/09/MOS_1280_kalel.jpg/300px-MOS_1280_kalel.jpg',
                                            )),
                                        index == 3
                                            ? Positioned(
                                            left: 30.0,
                                            child: Stack(
                                              alignment: Alignment.center,
                                              children: <Widget>[
                                                Container(
                                                  width: 40.0,
                                                  height: 15.0,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                      new BorderRadius
                                                          .circular(25.0),
                                                      color: Colors.grey[300]),
                                                ),
                                                Container(
                                                  height: 12.0,
                                                  width: 35.0,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                      new BorderRadius
                                                          .circular(25.0),
                                                      color: Colors.green),
                                                  child: Text(
                                                    ' 3 phút',
                                                    style: TextStyle(
                                                        fontSize: 10.0,
                                                        color: Colors.white),
                                                  ),
                                                )
                                              ],
                                            ))
                                            : Positioned(
                                            child: Stack(
                                              alignment: Alignment.center,
                                              children: <Widget>[
                                                Container(
                                                  width: 17.0,
                                                  height: 15.0,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                      new BorderRadius
                                                          .circular(25.0),
                                                      color: Colors.grey[300]),
                                                ),
                                                Container(
                                                  height: 12.0,
                                                  width: 12.0,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                      new BorderRadius
                                                          .circular(25.0),
                                                      color: Colors.green),
                                                )
                                              ],
                                            ))
                                      ],
                                    ),
                                  ),
                                ]),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 16.0),
                                  child: Container(
                                    alignment: Alignment.centerLeft,
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Padding(
                                          padding:
                                          const EdgeInsets.only(bottom: 3.0),
                                          child: Text(
                                            'User $index',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16),
                                          ),
                                        ),
                                        index == 1
                                            ? Text(
                                          'If youve ever wanted ',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16),
                                        )
                                            : index == 2
                                            ? Text(
                                          'seen',
                                          style: TextStyle(fontSize: 15),
                                        )
                                            : Text('no mess')
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              index == 1
                                  ? Padding(
                                padding: const EdgeInsets.only(right: 16.0),
                                child: Container(
                                  height: 12.0,
                                  width: 12.0,
                                  decoration: BoxDecoration(
                                      borderRadius:
                                      new BorderRadius.circular(25.0),
                                      color: Colors.blue),
                                ),
                              )
                                  : Padding(
                                padding: const EdgeInsets.only(right: 16.0),
                                child: index == 2
                                    ? CircleAvatar(
                                    radius: 6,
                                    backgroundImage: NetworkImage(
                                      'https://znews-photo.zadn.vn/w660/Uploaded/spuocaw/2017_10_04/KateWinsletinAvatar.jpg',
                                    ))
                                    : Icon(
                                  Icons.check_circle,
                                  color: Colors.grey[400],
                                  size: 16.0,
                                ),
                              ),
                            ],
                          )),
                    ));
              }, childCount: 50),
            ),
          ],
        ));
  }

  Widget build(BuildContext context) {
    PageController _myPage = PageController(initialPage: 0);

    return Scaffold(
        bottomNavigationBar: BottomAppBar(
          child: Container(
            height: 75,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: InkWell(
                      onTap: () {
                        _myPage.jumpToPage(0);
                        setState(() {
                          checkMess = true;
                          str = 'Chats';
                          this.checkContact = false;
                        });
                        print(checkContact);
                      },
                      child: Container(
                        width: 100.0,
                        child: Column(
                          children: <Widget>[
                            Stack(
                              overflow: Overflow.visible,
                              children: <Widget>[
                                checkMess
                                    ? Icon(
                                  Icons.message,
                                  size: 30.0,
                                  color: Colors.black,
                                )
                                    : Icon(
                                  Icons.message,
                                  color: Colors.grey[400],
                                  size: 30.0,
                                ),
                                Positioned(
                                    left: 20.0,
                                    child: Stack(
                                      alignment: Alignment.center,
                                      children: <Widget>[
                                        Container(
                                          alignment: Alignment.center,
                                          width: 17.0,
                                          height: 17.0,
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              border: Border.all(
                                                  color: Colors.white,
                                                  width: 2.0)),
                                        ),
                                        Container(
                                          alignment: Alignment.center,
                                          width: 14.0,
                                          height: 14.0,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.deepOrange,
                                          ),
                                          child: Text(
                                            '8',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 10.0),
                                          ),
                                        )
                                      ],
                                    ))
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Text('Chat'),
                            )
                          ],
                        ),
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: InkWell(
                      onTap: () {
                        _myPage.jumpToPage(1);
                        setState(() {
                          str = 'Danh ba';
                          this.checkContact = true;
                          this.checkMess = false;
                        });
                      },
                      child: Container(
                        width: 100.0,
                        child: Column(
                          children: <Widget>[
                            Stack(
                              overflow: Overflow.visible,
                              children: <Widget>[
                                checkContact
                                    ? Icon(
                                  Icons.account_box,
                                  size: 30.0,
                                  color: Colors.black,
                                )
                                    : Icon(
                                  Icons.account_box,
                                  color: Colors.grey[400],
                                  size: 30.0,
                                ),
                                Positioned(
                                    left: 20.0,
                                    child: Stack(
                                      children: <Widget>[
                                        Container(
                                          alignment: Alignment.center,
                                          width: 20.0,
                                          height: 17.0,
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: Colors.white),
                                        ),
                                        Container(
                                          alignment: Alignment.center,
                                          width: 20.0,
                                          height: 14.0,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.green,
                                          ),
                                          child: Text(
                                            '81',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 10.0),
                                          ),
                                        )
                                      ],
                                    ))
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Text(
                                'Danh ba',
                              ),
                            )
                          ],
                        ),
                      )),
                ),
              ],
            ),
          ),
        ),
        appBar: AppBar(
          elevation: 5.0,
          flexibleSpace: Container(
            height: 25.0,
            width: 25.0,
            color: Colors.green,
          ),
          backgroundColor: Colors.white,
          leading: Padding(
            padding: const EdgeInsets.only(left: 16.0, top: 8.0, bottom: 8.0),
            child: Container(
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.black,
                  image: DecorationImage(
                      image: NetworkImage(
                          'https://gamek.mediacdn.vn/2019/3/31/anh-1-1554010168855935071981.jpg',
                          scale: 6.0),
                      fit: BoxFit.fill)),
            ),
          ),
          title: Text(
            this.str,
            style: TextStyle(color: Colors.black),
          ),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 16.0),
              child: Container(
                  width: 38.0,
                  height: 38.0,
                  decoration: BoxDecoration(
                      color: Colors.grey[200], shape: BoxShape.circle),
                  child: Container(
                      child: Icon(
                        Icons.camera_alt,
                        color: Colors.black,
                        size: 16.0,
                      ))),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 16.0),
              child: Container(
                  width: 38.0,
                  height: 38.0,
                  decoration: BoxDecoration(
                      color: Colors.grey[200], shape: BoxShape.circle),
                  child: Container(
                      child: Icon(
                        Icons.edit,
                        color: Colors.black,
                        size: 16.0,
                      ))),
            ),
          ],
        ),
        body: PageView(
          controller: _myPage,
          children: <Widget>[_UiMess(), _UIContact()],
        ));
  }
}
