import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
String str= '';
String strok='';

  void initState() {
    _controller.addListener(() {
      final text = _controller.text.toLowerCase();
      _controller.value = _controller.value.copyWith(
        text: text,
        selection: TextSelection(baseOffset: text.length, extentOffset: text.length),
        composing: TextRange.empty,

      );
      setState(() {
        str = text;
print(str)
      ;
      });
    });

    super.initState();
  }

 final _controller = TextEditingController();
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(

          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new DropdownButton<String>(
              items: <String>['A', 'B', 'C', 'D'].map((String value) {
                return new DropdownMenuItem<String>(
                  value: value,
                  child:Text(value),
                );
              }).toList(),
              onChanged: (val) {
                setState(() {
                  this.str=val;
                  print(str);

                });
              },
            ),
           TextField(
             controller: _controller,
             enabled: false,
             decoration: InputDecoration(border: OutlineInputBorder(),hintText: this.str),
           ),

            InkWell(
               onTap: (){
                 setState(() {
                   this.strok = str;
                 });
               },
              child: Text('ok'),
            ),
            Text(
                this.strok
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
    );
  }
}
