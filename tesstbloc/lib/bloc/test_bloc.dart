import 'package:flutter_bloc/flutter_bloc.dart';
import 'dart:async';
import 'package:tesstbloc/bloc/test_bloc.dart';
import 'package:tesstbloc/bloc/test_event.dart';
import 'package:tesstbloc/bloc/test_state.dart';
class CountBloc extends Bloc<CountEvent,CounterState>{
  int count =0;
  @override
  // TODO: implement initialState
  CounterState get initialState => Inital();

  @override
  Stream<CounterState> mapEventToState(CountEvent event) async*{
    if(event is CounterIncreasing){
      this.count++;
      print(count);
      yield(CounterIncrease());
    }
    if(event is CounterReducting){
      this.count--;
      yield(CounterReduction());
    }
  }

}
